public class Animal {
    private String eats;
    public int noOfLegs;
    private String color;

    public void setQualities(String eats, int noOfLegs, String color) {
        this.eats = eats;
        this.noOfLegs = noOfLegs;
        this.color = color;
    }

    public String getEats() {
        return eats;
    }

    public String getColor() {
        return color;
    }
}
