import java.lang.reflect.Field;
import java.util.Scanner;

public class Cat {
    public static void main(String[] args) throws IllegalAccessException {
        Animal animal = new Animal();
        Scanner userInput = new Scanner(System.in);
        System.out.println("Про кого хочеш дізнатися: тварина чи кіт?");
        String answer = userInput.nextLine();
        if(answer.equals("тварина")) {
            interactWithAnimal(animal, "усе", 2, "білий");
            printInfoAnimal(animal);
            interactWithCat(animal, "рибу", 4, "чорний");
            printInfoAnimal(animal);
            userInput.close();
        }

        if(answer.equals("кіт")) {
            interactWithAnimal(animal, "усе", 2, "білий");
            interactWithCat(animal, "рибу", 4, "чорний");
            printInfoCat(animal);
            userInput.close();
        }
        if((!answer.equals("тварина")) && (!answer.equals("кіт"))) {
            System.out.println("Щось не те...");
            userInput.close();
        }
    }

    private static void interactWithAnimal(Animal animal, String eats, int noOfLegs, String color) {
        animal.setQualities(eats, noOfLegs, color);
    }

    private static void printInfoAnimal(Animal animal) {
        System.out.printf("Тварина їсть %s, має %d ноги й %s колір\n",
                animal.getEats(),
                animal.noOfLegs,
                animal.getColor());
    }

    private static void printInfoCat(Animal animal) {
        System.out.printf("Кіт їсть %s, має %d ноги й %s колір\n",
                animal.getEats(),
                animal.noOfLegs,
                animal.getColor());
    }

    private static void interactWithCat(Animal animal, String eats, int noOfLegs, String color) throws IllegalAccessException {
        Class<? extends Animal> animalClass = animal.getClass();
        for (Field field : animalClass.getDeclaredFields()) {
            if ("eats".equals(field.getName())) {
                field.setAccessible(true);
                field.get(animal);
                field.set(animal, eats);
            }
            if ("noOfLegs".equals(field.getName())) {
                field.get(animal);
                field.set(animal, noOfLegs);
            }
            if ("color".equals(field.getName())) {
                field.setAccessible(true);
                field.get(animal);
                field.set(animal, color);
            }
        }
    }
}
